class Board {

    // Creates new board representation based upon passed data, or it attempts to load data from specified REST api.
    constructor(data) {
        this.highlighted = null;
        if(data == null) {
            this.data = [];
            loadSudokuPuzzle(this);
        } else {
            this.data = data;
            renderBoard(data);
        }
    }

    saveGame() {
        if(window.navigator.userAgent.indexOf("Edge") == -1) {
            localStorage.setItem("board", JSON.stringify(this.data));
        }
    }

    getHighlighted() {
        return this.highlighted;
    }

    setHighlighted(cell) {
        this.highlighted = cell;
    }


    createBoard(data) {
        this.data = data;
        for (let i = 0; i < 9; i++) {
            for (let j = 0; j < 9 ; j++) {
                const value = data[i][j];
                if (value !=0) {
                    //Cells with nonzero values can't be changed.
                    this.data[i][j] = new Cell(data[i][j],true)
                } else {
                    this.data[i][j] = new Cell(data[i][j],false)
                }
            }
        }
        renderBoard(data);
        this.saveGame();
    }

    // Changes the value of cell based upon chosen number from number picker.
    setValueToHighlighted(value) {
        let lastChosenCell = board.getHighlighted();
        this.data[lastChosenCell.x][lastChosenCell.y].value = value;
        lastChosenCell.innerText = value;
        lastChosenCell.classList.remove('selected');
        this.setHighlighted(null);
        this.checkSolution();
        this.saveGame();
    }


    // Checks for working solution
    checkSolution() {
        if( this.checkRows() && this.checkColumns() && this.checkSquares()) {
            console.log("Congratulations!");
        }
    }

    checkRows() {
        for (let i = 0; i < 9; i++) {
            const set = new Set();
            for (let j = 0; j < 9; j++) {
                const value = this.data[i][j].value;
                if(value == 0) {
                    return false;
                } else {
                    set.add(value);
                }
            }

            if(set.length != 9) {
                // Set should contain all 9 numbers, otherwise some are missing or duplicate.
                return false;
            }
        }
    }

    checkColumns() {
        for (let i = 0; i < 9; i++) {
            const set = new Set();
            for (let j = 0; j < 9; j++) {
                const value = this.data[j][i].value;
                set.add(value);
            }
        }
        if(set.length != 9) {
            // Set should contain all 9 numbers, otherwise some are missing or duplicate.
            return false;
        }
    }

    checkSquares() {
        for (let i = 0; i < 3 ; i++) {
            for (let j = 0; j < 3 ; j++) {

                let set = new Set();
                for (let k = 0; k < 3; k++) {
                    for (let l = 0; l < 3; l++) {
                        const value = this.data[i*3 + k][j*3 + l].value;
                        set.add(value);
                    }
                }
                if(set.length != 9) {
                    // Set should contain all 9 numbers, otherwise some are missing or duplicate.
                    return false;
                }
            }
        }
    }


}

class Cell {
    constructor(value,rigid) {
        this.value = value;
        this.rigid = rigid;
    }
}


// Router makes this webpage into SPA.
class Router {
    constructor() {
        this.pages = document.querySelectorAll('section')
        this.route();
        window.addEventListener('popstate', e => this.route());
    }

    route() {
        const hash = window.location.hash;
        switch(hash) {
            case '#game': this.changePage(hash); break;
            case '#load': this.changePage(hash); break;
            case '': this.changePage(hash); break;

            default: this.changePage(''); break;
        }
    }

    changePage(section) {
        for(var page of this.pages) {
            if(page.getAttribute('data-route') == section) {
                page.classList.add('is-visible')
            } else {
                page.classList.remove('is-visible')
            }
        }
    }

}


// Creates the visualized sudoku grid, based upon board data.
function renderBoard(data) {
    const grid = document.querySelector("#grid");
    while (grid.firstChild) {
        grid.removeChild(grid.firstChild);
    }
    let squares = [];
    for(let i = 0; i < 3; i++) {
        squares[i] = [];
        for (let j = 0; j < 3; j++) {
            const square = document.createElement('div');
            square.classList.add('square');
            squares[i][j] = square;
        }
    }

    for (let i = 0; i < 9; i++) {
        for(let j = 0; j < 9; j++) {
            const square = squares[Math.floor(i/3)][Math.floor(j/3)];
            const cell = data[i][j];
            addBox(square, i, j, cell);
            grid.appendChild(square);
        }
    }
}


// Adds cell to square on board.
function addBox(square, x, y, cell) {
    const box = document.createElement('div');
    box.classList.add('cell');

    box.x = x;
    box.y = y;
    if(cell.value != 0) {
        box.innerText = cell.value;
        if(cell.rigid) {
            box.classList.add("rigid")
        }
    } else {
        box.addEventListener("click",e => cellHandler(e, box, x,y));
    }
    square.appendChild(box)
}


function cellHandler(e, box, x, y) {
    toggleModalState();
    if (!box.classList.contains("rigid")) {
        box.classList.add("selected");
        board.setHighlighted(box);
    }
}

// Loads sudoku puzzle from 3rd party API.
function sudokuPuzzleRequest() {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        xhr.addEventListener('load', e => {
            resolve(e);

        });
        xhr.addEventListener('error', e => {
            reject(e)
        });

        xhr.open('GET', "https://sugoku2.herokuapp.com/board?difficulty=random");
        xhr.send();
    });

}

function loadSudokuPuzzle(board) {
    sudokuPuzzleRequest()
        .then( e => {
            const response = e.target.responseText;
            const data = JSON.parse(response);
            board.createBoard(data.board);
        })
        .catch(e => console.log(e));
}

// Number picked event handler.
function numberChosen(value) {
    toggleModalState();
    board.setValueToHighlighted(value);
}


// Show/hide number picker.
function toggleModalState () {
    modalVisible = !modalVisible;
    if (modalVisible) {
        document.body.classList.add('modal-visible');
    } else {
        document.body.classList.remove('modal-visible');
    }
}



// Class that glues necessary event handlers, dynamic rendering and game logic together
// .
class SudokooApp {
    constructor() {
        this.addHandlersToButtons();
        this.createContinueButtonIfNeeded();
        new Router();
        this.createModalButtons();
    }

    addHandlersToButtons() {
        const newgamebutton = document.querySelector("#newgame");
        newgamebutton.addEventListener("click", evt => {
            if(window.navigator.userAgent.indexOf("Edge") == -1) {
                localStorage.clear();
            }
            board = new Board();
        });

        const dnd = document.querySelector("#dnd");
        dnd.addEventListener("drop", e => this.loadGame(e));
        dnd.addEventListener("dragover", e => e.preventDefault());
        const reloadGame = document.querySelector(".refresh");
        reloadGame.addEventListener("click", evt => {
            if(window.navigator.userAgent.indexOf("Edge") == -1) {
                localStorage.clear();
            }
            board = new Board();

        });
        const saveGame = document.querySelector(".save");
        saveGame.addEventListener("click", evt => {
            let a = document.createElement('a');
            a.setAttribute('href', 'data:text/plain;charset=utf-u,'+encodeURIComponent(JSON.stringify(board.data)));
            a.setAttribute('download', "sudokoo.json");
            a.click();
        });
        const playAudio = document.querySelector(".audio");
        playAudio.addEventListener("click", evt => {
            this.playMusic();
        });
        const muteAudio = document.querySelector(".mute");
        muteAudio.addEventListener("click", evt => {

            document.getElementById('background_music').pause();

        });
    }
    createContinueButtonIfNeeded() {
        if (data != null) {
            const menu = document.querySelector("#main");
            let continueButton = document.createElement("a");
            continueButton.innerText = "CONTINUE";
            continueButton.classList.add("button");
            continueButton.href = "#game";
            continueButton.addEventListener("click", evt => {
                this.playMusic()
            });
            menu.appendChild(continueButton);
        }
    }

    createModalButtons() {
        const modalContent = document.querySelector(".modal-content")
        for (let i = 1; i < 10; i++) {
            const button = document.createElement('button');
            button.innerText = i;
            button.addEventListener('click', e => numberChosen(i));
            modalContent.appendChild(button);
        }

    }

    loadGame(e) {
        e.stopPropagation();
        e.preventDefault();
        const files = e.dataTransfer.files;
        for (let i = 0, f; f = files[i]; i++) {
            if (!f.type.match('application/json')) {
                continue;
            }

            const fr = new FileReader();
            fr.addEventListener("load", e => {
                if(window.navigator.userAgent.indexOf("Edge") == -1) {
                    localStorage.clear();
                }
                board = new Board(JSON.parse(fr.result));
                let a = document.createElement('a');
                a.setAttribute('href', '#game');
                a.click();
            });
            fr.readAsText(f);
        }

    }

    playMusic() {
        document.getElementById('background_music').play();
    }

    download(content, fileName, contentType) {
        let a = document.createElement("a");
        let file = new Blob([content], {type: contentType});
        a.href = URL.createObjectURL(file);
        a.download = fileName;
        a.click();
    }
}

