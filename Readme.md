# Sudokoo
Tradiční hra sudoku, implementována pomocí klasického webového stacku -
HTML5, CSS3, JS. Samotné puzzly jsou generovány pomocí REST rozhraní z odkazu :
https://sugoku2.herokuapp.com/board?difficulty=random. 
Hra se po každém tahu ukládá do local storage prohlížeče a v případě pozdějšího
otevření stránky se pokusí tento save file najít. Ukládání hry není povoleno pro uživatele edge prohlížečů, protože 
 na většině testovaných edge zařízeních local storage nefunguje. Popřípadě je možno využít
tlačítka pro ukládání, který stáhne .json soubor. Ten je možno nahrát v main
menu pod sekcí load game pomocí jednoduchého přetažení souburu do vyhrazené
sekce.
Audio soubor přehrávaný na pozadí je možno ztlumit, popřípadě znovu zapnout
vyhrazenými tlačítky. Poslední tlačítko slouží k nahrání nového zadání.K
vykreslení tlačítek je použito SVG, a pro přechody mezi jednotlivými obrazovkami
jsou použity CSS přechody a jednoduché 2D transformace.
Media queries v projektu použity nejsou, ale vzhledem k tomu že je všechno
implementováno v flexboxu a gridboxu, stránka zůstává responzivní i na menších zařízeních. 